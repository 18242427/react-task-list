#!/bin/bash

if [ "$NODE_ENV" == "development" ]
then
  # In development mode, use nodemon to automatically restart the server when
  # code changes. We will also enable debugging.
  nodemon -L -x "node --use_strict --debug --nolazy" -e .js -w src src/server.js
else
  # In production mode, run node directly without any debugging.
  node --use_strict src/server.js
fi
